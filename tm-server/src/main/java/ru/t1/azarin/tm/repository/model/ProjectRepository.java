package ru.t1.azarin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    void deleteAllByUserId(@Nullable String userId);

    @Nullable
    List<Project> findAllByUserId(@NotNull String userId);

    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sortColumn")
    List<Project> findAllByUserIdWithSort(@Param("userId") String userId, @Param("sortColumn") @NotNull String sortColumn);

    @Nullable
    Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
