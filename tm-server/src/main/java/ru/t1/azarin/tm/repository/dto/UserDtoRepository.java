package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.UserDto;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

    void deleteById(@NotNull String id);

}


