package ru.t1.azarin.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

}
