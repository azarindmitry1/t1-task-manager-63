package ru.t1.azarin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.model.ISessionService;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.model.Session;
import ru.t1.azarin.tm.repository.model.SessionRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    public SessionRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public Session update(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsById(model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

}
