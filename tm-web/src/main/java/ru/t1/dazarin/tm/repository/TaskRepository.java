package ru.t1.dazarin.tm.repository;

import ru.t1.dazarin.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }


    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        for (int i = 1; i < 3; i++) {
            save(new Task("TASK-NAME " + i, "TASK-DESCRIPTION " + i));
        }
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        save(new Task("New task " + System.currentTimeMillis()));
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}
