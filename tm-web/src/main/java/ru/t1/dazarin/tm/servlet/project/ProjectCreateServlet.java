package ru.t1.dazarin.tm.servlet.project;

import ru.t1.dazarin.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public final class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ProjectRepository.getInstance().create();
        resp.sendRedirect("/projects");
    }

}
