package ru.t1.dazarin.tm.servlet.task;

import lombok.SneakyThrows;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.model.Task;
import ru.t1.dazarin.tm.repository.ProjectRepository;
import ru.t1.dazarin.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/update/*")
public final class TaskUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/task-update.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final Status status = Status.valueOf(req.getParameter("status"));
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateFinish");
        final String projectId = req.getParameter("projectId");

        final Task task = TaskRepository.getInstance().findById(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setProjectId(projectId);
        if (!dateStartValue.isEmpty()) task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else task.setDateStart(null);
        if (!dateFinishValue.isEmpty()) task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else task.setDateFinish(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
