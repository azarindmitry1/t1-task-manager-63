package ru.t1.dazarin.tm.repository;

import ru.t1.dazarin.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 3; i++) {
            save(new Project("PROJECT-NAME " + i, "PROJECT-DESCRIPTION " + i));
        }
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        save(new Project("New project " + System.currentTimeMillis()));
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}
