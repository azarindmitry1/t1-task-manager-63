<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>
    EDIT PROJECT: <u><c:out value="${project.name}"/></u>
</h1>

<div id="formEdit">
	<form action="/project/update?id=${project.id}" method="POST">
	    <input type="hidden" name="id" value="${project.id}" />
		<p>NAME: </p><input type="text" name="name" value="${project.name}" />
		<p>DESCRIPTION: </p><input type="text" name="description" value="${project.description}" />
		<p>STATUS:</p>
		<select name="status">
		    <c:forEach var="status" items="${statuses}">
                <option <c:if test="${project.status == status}">selected="selected"</c:if> value="${status}">${status.displayName}</option>
            </c:forEach>
		</select>
		<p>DATE START: </p><input type="date" name="dateStart" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStart}" />" />
		<p>DATE FINISH: </p><input type="date" name="dateFinish" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateFinish}" />" />
		<br>
		<br>
		<button type="submit">SAVE</button>
	</form>
</div>

<jsp:include page="../include/_footer.jsp"/>
