<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>
    EDIT TASK: <u><c:out value="${task.name}"/></u>
</h1>

<div id="formEdit">
	<form action="/task/update?id=${task.id}" method="POST">
	    <input type="hidden" name="id" value="${task.id}" />
		<p>NAME: </p><input type="text" name="name" value="${task.name}" />
		<p>DESCRIPTION: </p><input type="text" name="description" value="${task.description}" />
		<p>STATUS:</p>
		<select name="status">
		    <c:forEach var="status" items="${statuses}">
                <option <c:if test="${task.status == status}">selected="selected"</c:if> value="${status}">${status.displayName}</option>
            </c:forEach>
		</select>
		<p>PROJECT:</p>
		<select name="projectId">
			<option value="">-- // --</option>
			<c:forEach var="project" items="${projects}">
			    <option <c:if test="${project.id == task.projectId}">selected="selected"</c:if> value="${project.id}">${project.name}</option>
			</c:forEach>
		</select>
		<p>DATE START: </p><input type="date" name="dateStart" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateStart}" />" />
		<p>DATE FINISH: </p><input type="date" name="dateFinish" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateFinish}" />" />
		<br>
		<br>
		<button type="submit">SAVE</button>
	</form>
</div>

<jsp:include page="../include/_footer.jsp"/>
