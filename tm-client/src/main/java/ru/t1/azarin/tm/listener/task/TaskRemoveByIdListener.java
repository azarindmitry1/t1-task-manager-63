package ru.t1.azarin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    public final static String NAME = "task-remove-by-id";

    @NotNull
    public final static String DESCRIPTION = "Remove task by id.";

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.removeTaskByIdResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
