package ru.t1.azarin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    public final static String NAME = "task-show-by-id";

    @NotNull
    public final static String DESCRIPTION = "Show task by id.";

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskDto task = taskEndpoint.showTaskByIdResponse(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
