package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.service.ILoggerService;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.listener.AbstractListener;
import ru.t1.azarin.tm.util.SystemUtil;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.azarin.tm.command";

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Nullable
    @Autowired
    private AbstractListener[] abstractCommands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String param = args[0];
        applicationEventPublisher.publishEvent(new ConsoleEvent(param));
        return true;
    }

    public void processCommand() {
        try {
            System.out.println("ENTER THE COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            applicationEventPublisher.publishEvent(new ConsoleEvent(command));
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[ERROR]");
        }
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPid();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void run(@Nullable final String[] args) throws SQLException {
        if (processArgument(args)) System.exit(0);

        prepareStartup();

        while (true) {
            processCommand();
        }
    }

}